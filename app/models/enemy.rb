class Enemy < ApplicationRecord
  belongs_to :enemy
  has_many :missions

  def initialize(name='Ernst Stavro Blofeld')
    enemy = Enemy.find_by(name: name)
    @megalomaniac = enemy.megalomaniac
  end

  def megalomaniac?
    @megalomaniac
  end
end
