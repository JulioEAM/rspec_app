class Agent < ApplicationRecord
  belongs_to :agent
  has_many :missions

  def initialize(name='James Bond')
#    agent = Agent.find_by(name: name)
    agent = Agent.find(2)
#    p agent
#    p agent[:name]
    @name = agent[:name]
    @id   = agent[:id]
    @hero = agent[:hero]
    @number = agent[:number]

    missiones = Agent.find(agent[:id]).missions

    kills = 0
    missiones.each do |mission|
      @gadget = mission.gadget
      enemy = Enemy.find(mission[:enemy_id]) # Agent.find(agent[:id]).missions.enemy.find(mission.enemy_id)
      @enemy = enemy.name
      kills += 1 if mission.enemy_killed
      @mission = mission
    end

    @kill_count = kills

   end

  def create(name='Agente 007')
    @agent = Agent.find(name)
  end

  def favorite_gadget
    @gadget
  end

  def name
    @name
  end

  def id
    @id
  end

  def enemy
    @enemy
  end

  def hero?
    @hero
  end

  def number
    @number
  end

  def kill_count
    @kill_count
  end

  def mission
    @mission
  end

  def bond?
   @name == 'James Bond' && @number == '007'
  end

  def print_favorite_gadget
    if (name_sp = @name.split(/\W+/)).length > 1 then
      "Agent #{name_sp[1]} has a thing for #{@gadget}"
    else
    end
  end
end
