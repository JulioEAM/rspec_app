class Mission < ApplicationRecord
  belongs_to :agent
  has_one :enemy

  def initialize(agent_id = 2)
    mission = Mission.find_by(agent_id: agent_id)
    @mission = mission
  end

  def mission(agent_id)
    mission = Mission.find_by(agent_id: agent_id)
  end
end
