# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_29_205017) do

  create_table "agents", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "number"
    t.text "status"
    t.boolean "hero"
  end

  create_table "enemies", force: :cascade do |t|
    t.string "name"
    t.boolean "double_agent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "megalomaniac"
  end

  create_table "missions", force: :cascade do |t|
    t.text "name"
    t.text "status"
    t.integer "agent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "gadget"
    t.boolean "enemy_killed"
    t.integer "enemy_id"
    t.index ["agent_id"], name: "index_missions_on_agent_id"
    t.index ["enemy_id"], name: "index_missions_on_enemy_id"
  end

end
