class AddMegalomaniacToEnemies < ActiveRecord::Migration[5.2]
  def change
    add_column :enemies, :megalomaniac, :boolean
  end
end
