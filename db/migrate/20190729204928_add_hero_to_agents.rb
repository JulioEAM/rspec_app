class AddHeroToAgents < ActiveRecord::Migration[5.2]
  def change
    add_column :agents, :hero, :boolean
  end
end
