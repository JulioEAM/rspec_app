class CreateEnemies < ActiveRecord::Migration[5.2]
  def change
    create_table :enemies do |t|
      t.string :name
      t.boolean :double_agent

      t.timestamps
    end
  end
end
