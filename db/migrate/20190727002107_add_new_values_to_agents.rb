class AddNewValuesToAgents < ActiveRecord::Migration[5.2]
  def change
    add_column :agents, :number, :int
    add_column :agents, :status, :text
  end
end
