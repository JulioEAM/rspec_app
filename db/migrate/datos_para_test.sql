BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `missions` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	text,
	`status`	text,
	`agent_id`	integer,
	`created_at`	datetime NOT NULL,
	`updated_at`	datetime NOT NULL,
	`gadget`	varchar,
	`enemy_killed`	boolean,
	`enemy_id`	integer,
	CONSTRAINT `fk_rails_5bd53528f0` FOREIGN KEY(`agent_id`) REFERENCES `agents`(`id`)
);
INSERT INTO `missions` VALUES (1,'Bahía de Cochinos','Cerrada',2,'2019-07-26 01:17:07','2019-07-26 01:17:07','Glog',1,2);
INSERT INTO `missions` VALUES (2,'Blood Brothers','Cerrada',2,'2019-07-26 01:28:54','2019-07-26 01:28:54','MP40',0,2);
INSERT INTO `missions` VALUES (3,'Train','Cerrada',3,'2019-07-26 01:30:35','2019-07-26 01:30:35','Walther PPK',0,1);
INSERT INTO `missions` VALUES (4,'RunWay','Cancelada',3,'2019-07-26 01:31:16','2019-07-26 01:31:16','Walther PPK',1,1);
CREATE TABLE IF NOT EXISTS `enemies` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar,
	`double_agent`	boolean,
	`created_at`	datetime NOT NULL,
	`updated_at`	datetime NOT NULL
);
INSERT INTO `enemies` VALUES (1,'Ernst Stavro Blofeld',1,'2019-07-29 19:02:46','2019-07-29 19:02:46');
INSERT INTO `enemies` VALUES (2,'Raúl Castro',0,'2019-07-29 19:02:46','2019-07-29 19:02:46');
CREATE TABLE IF NOT EXISTS `agents` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar,
	`created_at`	datetime NOT NULL,
	`updated_at`	datetime NOT NULL,
	`number`	int,
	`status`	text
);
INSERT INTO `agents` VALUES (2,'Agente ejemplo','2019-07-26 01:13:13','2019-07-26 01:13:13',1,'Active');
INSERT INTO `agents` VALUES (3,'James Bond','2019-07-26 01:14:06','2019-07-26 01:14:06',7,'Active forever');
INSERT INTO `agents` VALUES (4,'WorstAgent','2019-07-29 03:15:06','2019-07-29 03:15:06',0,'Dead');
CREATE INDEX IF NOT EXISTS `index_missions_on_enemy_id` ON `missions` (
	`enemy_id`
);
CREATE INDEX IF NOT EXISTS `index_missions_on_agent_id` ON `missions` (
	`agent_id`
);
COMMIT;
