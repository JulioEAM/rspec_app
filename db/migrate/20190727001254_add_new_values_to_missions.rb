class AddNewValuesToMissions < ActiveRecord::Migration[5.2]
  def change
    add_column :missions, :enemy_killed, :boolean
    add_reference :missions, :enemy, foreign_key: true
  end
end
