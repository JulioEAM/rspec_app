require 'rails_helper'

RSpec.describe Agent, type: :model do
  describe Agent, '#favorite_gadget' do
    it 'returns one item, the favorite gadget of the agent ' do
    # Setup
      agent_object = Agent.new('James Bond')

    # Exercise
      favorite_gadget = agent_object.favorite_gadget

    # Verify
      expect(favorite_gadget).to eq 'Walther PPK'

    # Teardown is mostly handled by RSpec itself
    end
  end

  describe Agent, '#agent_enemy_name' do
    it 'tells if the enemy was Ernst Stavro Blofeld' do
      agent = Agent.new('James Bond')

      expect(agent.enemy).to eq 'Ernst Stavro Blofeld'
    end
  end

  describe Agent, '#agent_enemy_wrong_name' do
    it 'tells if the enemy wasnt Winnie the Pooh' do
      agent = Agent.new('James Bond')

      expect(agent.enemy).not_to eq 'Winnie Pooh'
    end
  end

  describe Agent, '#agent_hero_or_not_hero' do
    before(:each) do
      @agent_ok = Agent.new('James Bond')
      @agent_nok = Agent.new('WorstAgent')
    end

    it 'tells if agent is a hero' do
      expect(@agent_ok.hero?).to be_truthy
    end

    it 'tells if agent is not a hero' do
      expect(@agent_nok.hero?).to be_falsy
    end
  end

  describe Enemy, '#enemy_megalomaniac' do
    it 'tells if enemy is megalomaniac' do
      enemy = Enemy.new('Raúl Castro')

      expect(enemy.megalomaniac?).to be true
    end
  end

  describe Enemy, '#enemy_not_megalomaniac' do
    it 'tells if enemy is not megalomaniac' do
      enemy = Enemy.new('Ernst Stavro Blofeld')

      expect(enemy.megalomaniac?).to be false
    end
  end

  describe Agent, '#agent_obj_not_nil' do
    it 'tells if object from agent is not nil' do
      agent = Agent.new('WorstAgent')

      expect(agent).to_not be_nil
    end
  end

  describe Enemy, '#enemy_obj_not_nil' do
    it 'tells if object from enemy is not nil' do
      enemy = Enemy.new('Ernst Stavro Blofeld')

      expect(enemy).to_not be_nil
    end
  end

  describe Agent, '#agent_number_long_&_type' do
    it 'tells if the number from agent is 3 chars long and numeric type' do
      agent = Agent.new('James Bond')

      expect(agent.number).to match(/\d{3}/)
    end
  end

  describe Agent, '#agent_wrong_number_long_&_type' do
    it 'tells when the number from agent is not 3 chars long and numeric type' do
      agent = Agent.new('WorstAgent')

      expect(agent.number).to_not match(/\d{3}/)
    end
  end

  describe Agent, :k_counter do
    it 'tells when an agent has at least a kill' do
      agent = Agent.new('James Bond')

      expect(agent.kill_count).to be >= 1
    end
  end

# Buscar qué significa << (agent.mission << mission)

#  describe Agent, '#enemy_not_instance_agent' do
#    it 'validates that enemy is not an instance of agent' do
#      agent = Agent.new('James Bond')
#      mission = Mission.new(agent.id)
#      p agent.mission
#      agent.mission << mission
#
#
#      expect(agent.mission).not_to be_an_instance_of(Mission)
#    end
#  end

# ¿Porqué fallan los errores?
#  describe Agent, '#agent_errors' do
#    it 'gives a message about some errors' do
#      agent = Agent.new('James Bond')
#
#      expect(agent.lady_killer?).to raise_error(NoMethodError)
#      expect(double_agent.name).to raise_error(NameError)
#      expect(double_agent.name).to raise_error("Error: No double agents around")
#      expect(double_agent.name).to raise_error(NameError, "Error: No double agents around")
#    end
#  end

  describe Agent, :bond do
    before(:all) do
      @agent_b = Agent.new('James Bond')
      @agent_no_b = Agent.new('WorstAgent')
    end

    it 'tells if an agent is Bond' do
      expect(@agent_b).to be_bond
    end 

    it 'tells if an agent is not Bond' do
      expect(@agent_no_b).not_to be_bond
    end
  end

## Esta onda de Objetos
#  describe Mission, '#prepare', :let do
#    let(:mission)  { Mission.create(name: 'Moonraker') }
#    let!(:bond)    { Agent.create(name: 'James Bond') }
#  
#    it 'adds agents to a mission' do
#      mission.prepare(bond)
#      expect(mission.agents).to include bond
#    end
#  end

## No funcionó subject
#  describe Agent, '#status' do
#    subject { Agent.new('James Bond')  }
#  
#    it 'returns the agents status' do
#      p subject.status
#      expect(subject.status).not_to be 'MIA'
#    end
#  end

  describe Agent, :print_f_g do
    it 'prints out the agents name, rank and favorite gadget' do
      agent = Agent.new('James Bond')
    
      expect(agent.print_favorite_gadget).to eq('Agent Bond has a thing for Walther PPK')
    end
  end

  describe Agent, :bond_extract do
    it 'tells if an agent is Bond' do
      agent = build_agent('James Bond')
      expect(agent).to be_bond
    end 

    it 'tells if an agent is not Bond' do
      agent = build_agent('WorstAgent')
      expect(agent).not_to be_bond
    end

    def build_agent(name='Agente')
      Agent.new(name)
    end
  end

  describe "agent shoulda_matchers", :bond_matchers do
    subject { agent = Agent.new('James Bond') }
    it { is_expected.to be_bond}
  end

  describe "agent not bond shoulda_matchers", :bond_matchers do
    subject { agent = Agent.new('WorstAgent') }
    it { is_expected.not_to be_bond}
  end

# FactoryBot

  describe "agent factory_bot", :bond_factory do
    let(:agent) { FactoryBot.create(:agent) }
#    subject { agent }
    it { p agent }
  end

end