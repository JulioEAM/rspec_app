FactoryBot.define do
  factory :agent do
    name { "James Bond" }
    hero  { true }
    kill_count { 200000 }
  end
end
